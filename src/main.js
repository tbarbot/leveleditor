import Vue from 'vue'
import App from './App.vue'


window.addEventListener("keydown", function (e) {
  // space and arrow keys
  if ([38, 40].indexOf(e.keyCode) > -1) {
    e.preventDefault();
  }
}, false);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
